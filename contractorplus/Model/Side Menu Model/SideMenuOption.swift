//
//  SideMenuOptions.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 30/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

struct SideMenuOption {
    let icon: UIImage
    let title: String
}

let sideMenuOptionsArr = [
    SideMenuOption(icon: SideMenuImages.LEADS!, title: "Leads"),
    SideMenuOption(icon: SideMenuImages.CLIENT!, title: "Clients"),
    SideMenuOption(icon: SideMenuImages.TEAM!, title: "Team"),
    SideMenuOption(icon: SideMenuImages.ESTIMATE!, title: "Estimates"),
    SideMenuOption(icon: SideMenuImages.INVOICE!, title: "Invoices"),
    SideMenuOption(icon: SideMenuImages.PROJECT!, title: "Projects"),
    SideMenuOption(icon: SideMenuImages.SUPPLIES!, title: "Supplies"),
    SideMenuOption(icon: SideMenuImages.TOOLS!, title: "Tools"),
    SideMenuOption(icon: SideMenuImages.TASK!, title: "Tasks"),
    SideMenuOption(icon: SideMenuImages.EXPENSES!, title: "Expenses"),
    SideMenuOption(icon: SideMenuImages.TIME_SHEET!, title: "Timesheet"),
    SideMenuOption(icon: SideMenuImages.MILEAGE_LOG!, title: "Mileage Log"),
    SideMenuOption(icon: SideMenuImages.POST_INSPECTION!, title: "Post Inspection"),
    SideMenuOption(icon: SideMenuImages.DIRECTORY!, title: "Directory"),
]

 //Team, Supplies & Expenses 
