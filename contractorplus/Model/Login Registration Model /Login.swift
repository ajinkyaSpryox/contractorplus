//
//  Login.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 06/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import Foundation

// MARK: - Login
struct Login: Codable {
    let success: Bool
    let message: String
    let data: LoginData
}

// MARK: - LoginData
struct LoginData: Codable {
    let tenantID, staffid, email, firstname: String
    let lastname: String
    let password, datecreated: String
    let lastIP, lastLogin, lastActivity: String
    let admin: String
    let active, defaultLanguage: String
    let isNotStaff, hourlyRate, twoFactorAuthEnabled: String

    enum CodingKeys: String, CodingKey {
        case tenantID = "tenant_id"
        case staffid, email, firstname, lastname, password, datecreated
        case lastIP = "last_ip"
        case lastLogin = "last_login"
        case lastActivity = "last_activity"
        case admin, active
        case defaultLanguage = "default_language"
        case isNotStaff = "is_not_staff"
        case hourlyRate = "hourly_rate"
        case twoFactorAuthEnabled = "two_factor_auth_enabled"
    }
}
