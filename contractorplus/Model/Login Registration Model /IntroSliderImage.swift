//
//  IntroSliderImage.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 04/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

struct SliderImage {
    let sliderImageName: UIImage
}

let sliderImagesArr = [
    SliderImage(sliderImageName: #imageLiteral(resourceName: "slider-image1")),
    SliderImage(sliderImageName: #imageLiteral(resourceName: "slider-image2")),
    SliderImage(sliderImageName: #imageLiteral(resourceName: "slider-image3")),
    SliderImage(sliderImageName: #imageLiteral(resourceName: "slider-image4"))
]
