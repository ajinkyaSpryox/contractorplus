//
//  Common Methods.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 30/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

func setupNavigationBarUI() {
    UINavigationBar.appearance().barTintColor = #colorLiteral(red: 0.9097405076, green: 0.1442055702, blue: 0.1389430463, alpha: 1)
    UINavigationBar.appearance().tintColor = .white
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    UINavigationBar.appearance().isTranslucent = false
}

func setView(view: UIView, hidden: Bool, durationTime: Double) {
    UIView.transition(with: view, duration: durationTime, options: .transitionCrossDissolve, animations: {
        view.isHidden = hidden
    })
    
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
