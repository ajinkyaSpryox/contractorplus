//
//  WebServiceManager.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 06/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import Foundation
import Alamofire

class WebServiceManager {
    static let instance = WebServiceManager()
    lazy var connectivityManager = NetworkReachabilityManager()
    lazy var sessionManager = URLSessionConfiguration.default
    
    
    func getWebServiceDataFor <T: Codable> (url: String,
                                            method: HTTPMethod,
                                            parameters: Parameters? = nil,
                                            encodingType: ParameterEncoding,
                                            headers: HTTPHeaders? = nil,
                                            completion: @escaping(Result<T, WebServiceErrorMessages>) -> Void) {
        
        sessionManager.timeoutIntervalForRequest = 10
        
        if connectivityManager?.isReachable ?? false {
            
            AF.request(url, method: method, parameters: parameters, encoding: encodingType, headers: headers)
                .validate().responseJSON(completionHandler: { response in
                
                if response.error != nil {
                    completion(.failure(.NO_INTERNET_AVAILABLE))
                }
                else {
                    guard let data = response.data else {return completion(.failure(.UNABLE_TO_RETRIVE_DATA))}
                    let decoder = JSONDecoder()
                    
                    do{
                        let returnedResponse = try decoder.decode(T.self, from: data)
                        completion(.success(returnedResponse))
                    }catch{
                        completion(.failure(.UNABLE_TO_RETRIVE_DATA))
                    }
                    
                }
                
            })
            
        }
        else {
            completion(.failure(.NO_INTERNET_AVAILABLE))
        }
        
    }
}
