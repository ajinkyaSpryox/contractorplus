//
//  Constants.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 29/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

struct StoryBoardNames {
    static let LOGIN = UIStoryboard(name: "Login", bundle: nil)
    static let SIDEMENU = UIStoryboard(name: "SideMenu", bundle: nil)
    static let CLIENT = UIStoryboard(name: "Client", bundle: nil)
    static let CLIENT_DETAIL_SEGMENTS = UIStoryboard(name: "ClientDetailSegments", bundle: nil)
    static let ESTIMATE = UIStoryboard(name: "Estimate", bundle: nil)
    static let ESTIMATE_INFO_SEGMENTS = UIStoryboard(name: "EstimateInfoSegments", bundle: nil)
    static let COMMON_SCREENS = UIStoryboard(name: "CommonScreens", bundle: nil)
    static let SHOPPING_LIST = UIStoryboard(name: "ShoppingList", bundle: nil)
}

enum AppImages {
    static let SEARCH = UIImage(named: "search")
    static let MENU = UIImage(named: "menu")
    static let DEFAULT_AVATAR = UIImage(named: "default-avatar")
    static let UNSELECT_CHECKMARK = UIImage(named: "checkbox-unselected")
    static let SELECTED_CKECKMARK = UIImage(named: "checkbox-selected")
    static let COMPANY_ICON = UIImage(named: "company-icon")
    static let EMAIL_ICON = UIImage(named: "email-icon")
    static let DELETE_BIN_RED = UIImage(named: "delete-bin-red")
    static let AVATAR_TEAL = UIImage(named: "avatar-teal")
    static let CLOSE = UIImage(named: "close")
    static let BACK_ARROW_WHITE = UIImage(named: "back-arrow-white")
    static let BACK_ARROW_BLACK = UIImage(named: "back-arrow-black")
    static let RIGHT_FORWARD_RED = UIImage(named: "right-forward-red")
    static let MAIL_WHITE = UIImage(named: "mail-white")
}

enum SideMenuImages {
    static let CLIENT = UIImage(named: "client")
    static let ESTIMATE = UIImage(named: "estimate")
    static let PROJECT = UIImage(named: "project")
    static let INVOICE = UIImage(named: "invoice")
    static let TASK = UIImage(named: "task")
    static let POST_INSPECTION = UIImage(named: "post-inspection")
    static let TIME_SHEET = UIImage(named: "timesheet")
    static let MILEAGE_LOG = UIImage(named: "mileage-log")
    static let TOOLS = UIImage(named: "tool")
    static let DIRECTORY = UIImage(named: "directory")
    static let LEADS = UIImage(named: "leads")
    static let SUPPLIES = UIImage(named: "supplies")
    static let TEAM = UIImage(named: "teams")
    static let EXPENSES = UIImage(named: "expenses")
    
}

enum WebServiceErrorMessages: String, Error {
    case NO_INTERNET_AVAILABLE = "No internet available, try again later."
    case UNABLE_TO_RETRIVE_DATA = "Unable to retrive data from client."
    case WRONG_CREDENTAILS = "The username or password you have entered is invalid"
}

enum AppErrorMessages: String {
    case EMAIL_PASSWORD_REQUIRED = "Email & Password is required."
}

struct AppUrls {
    static let BASE_URL = "https://dev.my.contractorplus.app/"
    static let LOGIN_URL = BASE_URL + "api/login/auth"
}

struct AppCredentials {
    static let GOOGLE_CLIENT_ID = "101854247875-onn29va9sq7a9spmjhrfplj0n5rl7crk.apps.googleusercontent.com"
}
