//
//  AddShoppingListItemsVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 06/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class AddShoppingListItemsVC: UIViewController {
    
    //Searched Items Outlets
    @IBOutlet weak var searchedItemStack: UIStackView!
    @IBOutlet weak var searchedItemTableView: UITableView!
    @IBOutlet weak var searchedItemTableHeight: NSLayoutConstraint!
    
    //Added Items Outlets
    @IBOutlet weak var addedItemStack: UIStackView!
    @IBOutlet weak var addedItemTableView: UITableView!
    @IBOutlet weak var addedItemTableHeight: NSLayoutConstraint!
    
    //Text Field Outlets
    @IBOutlet weak var shoppingListTitleTextField: UITextField!
    @IBOutlet weak var searchItemTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        title = "Add Item"
        addNavigationBarButtons()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func addNavigationBarButtons() {
        let rightnavigationSaveBtn = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveBtnTapped(_:)))
        navigationItem.rightBarButtonItems = [rightnavigationSaveBtn]
    }
    
    private func setupInitialUI() {
        searchedItemStack.isHidden = true
        addedItemStack.isHidden = true
        searchItemTextField.delegate = self
        shoppingListTitleTextField.delegate = self
        
        searchedItemTableView.delegate = self
        searchedItemTableView.dataSource = self
        searchedItemTableView.register(UINib(nibName: "ShoppingListItemsSearchTblCell", bundle: nil), forCellReuseIdentifier: "ShoppingListItemsSearchTblCell")
        
        addedItemTableView.delegate = self
        addedItemTableView.dataSource = self
        addedItemTableView.register(UINib(nibName: "ShoppingListItemAddTblCell", bundle: nil), forCellReuseIdentifier: "ShoppingListItemAddTblCell")
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        searchedItemTableHeight.constant = searchedItemTableView.contentSize.height
        addedItemTableHeight.constant = addedItemTableView.contentSize.height
    }

}

//MARK: - IB ACTIONS IMPLEMENTATION
extension AddShoppingListItemsVC {
    
    @objc
    func saveBtnTapped(_ sender: UIBarButtonItem) {
        
    }
    
}

//MARK: TEXT FIELD DELEGATE IMPLEMENTATION
extension AddShoppingListItemsVC: UITextFieldDelegate {
    
    @IBAction func textFieldEditingChanged(_ textfield: UITextField) {
        switch textfield {
        case searchItemTextField:
            searchedItemStack.isHidden = textfield.text == ""
            addedItemStack.isHidden = textfield.text == ""
        default:
            return
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        return true
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension AddShoppingListItemsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case searchedItemTableView:
            return 3
        case addedItemTableView:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        
        case searchedItemTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingListItemsSearchTblCell") as? ShoppingListItemsSearchTblCell else {return UITableViewCell()}
            cell.configureCell()
            return cell
            
        case addedItemTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingListItemAddTblCell") as? ShoppingListItemAddTblCell else {return UITableViewCell()}
            cell.configureCell()
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
