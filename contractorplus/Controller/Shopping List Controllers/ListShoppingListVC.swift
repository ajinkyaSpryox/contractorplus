//
//  ListShoppingListVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 06/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class ListShoppingListVC: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        title = "Shopping List"
        addNavigationBarButtons()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("List Shopping List VC")
    }
    
    private func setupInitialUI() {
        
    }
    
    private func addNavigationBarButtons() {
        let rightNavigationSearchBarButton = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(searchBtnTapped(_:)))
        rightNavigationSearchBarButton.image = AppImages.SEARCH
        navigationItem.rightBarButtonItems = [rightNavigationSearchBarButton]
    }

}

extension ListShoppingListVC {
    
    @objc
    func searchBtnTapped(_ sender: UIBarButtonItem) {
        #warning("Search functionality here")
        guard let addShoppingItemsVC = StoryBoardNames.SHOPPING_LIST.instantiateViewController(withIdentifier: "AddShoppingListItemsVC") as? AddShoppingListItemsVC else {return}
        navigationController?.pushViewController(addShoppingItemsVC, animated: true)
    }
    
}
