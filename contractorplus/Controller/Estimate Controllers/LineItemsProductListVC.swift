//
//  CommonLineItemsProductVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 02/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class LineItemsProductListVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var topTitleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lineItemProductsTableView: UITableView!
    @IBOutlet weak var searchLineItemTextField: UITextField!
    
    //Search Line Item Stack Outlets
    @IBOutlet weak var searchLineItemStack: UIStackView!
    @IBOutlet weak var searchedLineItemLbl: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        topTitleViewHeight.constant = UIDevice.current.hasNotch ? 80 : 60
        searchLineItemStack.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        lineItemProductsTableView.delegate = self
        lineItemProductsTableView.dataSource = self
        lineItemProductsTableView.register(UINib(nibName: "LineItemsProductTblCell", bundle: nil), forCellReuseIdentifier: "LineItemsProductTblCell")
        searchLineItemTextField.delegate = self
    }

}

//MARK: - IB ACTIONS IMPLEMENTATION
extension LineItemsProductListVC {
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addNewLineItemBtnTapped(_ sender: UIButton) {
        guard let addNewLineItemVC = StoryBoardNames.ESTIMATE.instantiateViewController(withIdentifier: "AddNewLineItemVC") as? AddNewLineItemVC else {return}
        navigationController?.pushViewController(addNewLineItemVC, animated: true)
    }
    
}

//MARK: - TEXT FIELD DELEGATE IMPLEMENTATION
extension LineItemsProductListVC: UITextFieldDelegate {
    
    @IBAction func textFieldEditingChanged(_ textfield: UITextField) {
        guard let searchedText = textfield.text else {return}
        searchedLineItemLbl.text = "\"\(searchedText.uppercased())\""
        searchLineItemStack.isHidden = searchedText == ""
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        return true
    }
    
}

//MARK: - TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension LineItemsProductListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LineItemsProductTblCell") as? LineItemsProductTblCell else {return UITableViewCell()}
        cell.configureCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
