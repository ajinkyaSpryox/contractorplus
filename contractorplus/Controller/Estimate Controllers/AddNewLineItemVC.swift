//
//  AddNewLineItemVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 02/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class AddNewLineItemVC: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "NEW LINE ITEM"
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
    }
    
    private func initialUISetup() {
        addNavigationBarButtonItems()
    }
    
    private func addNavigationBarButtonItems() {
        let rightNavigationBtn = UIBarButtonItem(barButtonSystemItem: .save,
                                                 target: self,
                                                 action: #selector(saveBtnTapped(_:)))
        navigationItem.rightBarButtonItem = rightNavigationBtn
    }

}

//MARK: - IB ACTIONS IMPLEMEMTATION
extension AddNewLineItemVC {
    
    @objc func saveBtnTapped(_ sender: UIBarButtonItem) {
        print("Save Button Tapped")
    }
}
