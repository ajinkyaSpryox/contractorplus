//
//  SegmentedEstimateLineItemVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 06/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class SegmentedEstimateLineItemVC: UIViewController {
    
    @IBOutlet weak var segmentedLineItemsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        segmentedLineItemsTableView.delegate = self
        segmentedLineItemsTableView.dataSource = self
        segmentedLineItemsTableView.register(UINib(nibName: "SegmentedEstimateLineItemTblCell", bundle: nil), forCellReuseIdentifier: "SegmentedEstimateLineItemTblCell")
    }

}

//MARK: - TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension SegmentedEstimateLineItemVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SegmentedEstimateLineItemTblCell") as? SegmentedEstimateLineItemTblCell else {return UITableViewCell()}
        cell.configureCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}
