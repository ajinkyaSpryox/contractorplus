//
//  SegmentedEsitmateMessageVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 05/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class SegmentedEsitmateMessageVC: UIViewController {
    
    @IBOutlet weak var estimateMessageTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        estimateMessageTableView.delegate = self
        estimateMessageTableView.dataSource = self
        estimateMessageTableView.register(UINib(nibName: "SegmentEstimateMessageTblCell", bundle: nil), forCellReuseIdentifier: "SegmentEstimateMessageTblCell")
    }

}

extension SegmentedEsitmateMessageVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SegmentEstimateMessageTblCell") as? SegmentEstimateMessageTblCell else {return UITableViewCell()}
        cell.configureCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
