//
//  EstimateInfoVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 05/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit
import CarbonKit

class EstimateInfoVC: UIViewController {
    
    @IBOutlet weak var topDisplayView: UIView!
    @IBOutlet weak var topDisplayViewHeight: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        setupCarbonKit()
    }

}

//MARK: - IB ACTIONS IMPLEMENTATION
extension EstimateInfoVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

//MARK: - CARBON TAB SETUP & DELEGATE IMPLEMENTATION
extension EstimateInfoVC: CarbonTabSwipeNavigationDelegate {
   
    private func setupCarbonKit() {
        let menuItems = ["DETAILS", "MESSAGES", "LINE ITEMS"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: menuItems, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.view.translatesAutoresizingMaskIntoConstraints = false
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = .white
        carbonTabSwipeNavigation.setTabBarHeight(50)
        carbonTabSwipeNavigation.setIndicatorColor(.systemRed)
        carbonTabSwipeNavigation.setNormalColor(.secondaryLabel, font: UIFont(name: "MyriadPro-Semibold", size: 16)!)
        carbonTabSwipeNavigation.setSelectedColor(#colorLiteral(red: 0.9647058824, green: 0.02352941176, blue: 0.1254901961, alpha: 1), font: UIFont(name: "MyriadPro-Semibold", size: 16)!)
        
        for (index, _) in menuItems.enumerated() {
            carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / CGFloat(menuItems.count), forSegmentAt: index)
        }
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation.view ?? "", attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: topDisplayView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: topDisplayViewHeight.constant))
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation.view ?? "", attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view , attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0))
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        switch index {
        case 0:
            //Display Segmented Profile View Controller
            guard let segmentedEstimateDetailVC = StoryBoardNames.ESTIMATE_INFO_SEGMENTS.instantiateViewController(withIdentifier: "SegmentedEstimateDetailVC") as? SegmentedEstimateDetailVC else {return UIViewController()}
            return segmentedEstimateDetailVC
            
        case 1:
            //Display Segmented Message View Contrller
            guard let segmentedEstimatedMessageVC = StoryBoardNames.ESTIMATE_INFO_SEGMENTS.instantiateViewController(withIdentifier: "SegmentedEsitmateMessageVC") as? SegmentedEsitmateMessageVC else {return UIViewController()}
            return segmentedEstimatedMessageVC
            
        case 2:
            //Display Segmented Line Items View Controller
            guard let segmentedEstimateLineItemVC = StoryBoardNames.ESTIMATE_INFO_SEGMENTS.instantiateViewController(withIdentifier: "SegmentedEstimateLineItemVC") as? SegmentedEstimateLineItemVC else {return UIViewController()}
            return segmentedEstimateLineItemVC
            
        default:
            return UIViewController()
        }
        
    }
    
}
