//
//  ListEstimateVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 05/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class ListEstimateVC: UIViewController {
    
    //Outlets
    var floatingActionBtn: ActionButton!
    @IBOutlet weak var listEstimateTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        title = "Estimates"
        addNavigationBarButtonItems()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        setupFloatingActionButton()
        listEstimateTableView.delegate = self
        listEstimateTableView.dataSource = self
        listEstimateTableView.register(UINib(nibName: "ListEstimateTblCell", bundle: nil), forCellReuseIdentifier: "ListEstimateTblCell")
    }
    
    private func setupFloatingActionButton() {
        floatingActionBtn = ActionButton(attachedToView: self.view, items: nil)
        floatingActionBtn.setTitle("+", forState: .normal)
        floatingActionBtn.backgroundColor = #colorLiteral(red: 0.9097405076, green: 0.1442055702, blue: 0.1389430463, alpha: 1)
        floatingActionBtn.action = { [weak self] action in
            guard let self = self else {return}
            guard let addNewEstimateVC = StoryBoardNames.ESTIMATE.instantiateViewController(withIdentifier: "NewEstimateVC") as? NewEstimateVC else {return}
            self.navigationController?.pushViewController(addNewEstimateVC, animated: true)
        }
    }
    
    private func addNavigationBarButtonItems() {
        let rightSearchNavigationBtn = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(searchBtnTapped(_:)))
        rightSearchNavigationBtn.image = AppImages.SEARCH
        navigationItem.rightBarButtonItems = [rightSearchNavigationBtn]
    }

}

//MARK: - IB ACTIONS IMPLEMENTATION
extension ListEstimateVC {
    
    @objc
    func searchBtnTapped(_ sender: UIBarButtonItem) {
        #warning("Navigation to common Search - to be planned")
    }
    
}

//MARK: - TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension ListEstimateVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListEstimateTblCell") as? ListEstimateTblCell else {return UITableViewCell()}
        cell.configureCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let estimateInfoVC = StoryBoardNames.ESTIMATE.instantiateViewController(withIdentifier: "EstimateInfoVC") as? EstimateInfoVC else {return}
        navigationController?.pushViewController(estimateInfoVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
