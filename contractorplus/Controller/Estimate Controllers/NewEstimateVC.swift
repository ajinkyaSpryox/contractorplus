//
//  NewEstimateVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 01/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class NewEstimateVC: UIViewController {
    
    //Client Outlets
    @IBOutlet weak var clientNameLbl: UILabel!
    @IBOutlet weak var addClientBtn: UIButton!
    @IBOutlet weak var clientChangeBtn: UIButton!
    
    //Line Item Table Outlets
    @IBOutlet weak var lineItemDetailsTableView: UITableView!
    @IBOutlet weak var lineItemTableViewHeight: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Create Estimate"
        hideUnhideClientViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        addNavigationBarButtonItems()
        lineItemDetailsTableView.delegate = self
        lineItemDetailsTableView.dataSource = self
        lineItemDetailsTableView.register(UINib(nibName: "LineItemDetailTblCell", bundle: nil), forCellReuseIdentifier: "LineItemDetailTblCell")
    }
    
    private func addNavigationBarButtonItems() {
        let rightNavigationBtn = UIBarButtonItem(barButtonSystemItem: .save,
                                                 target: self,
                                                 action: #selector(saveBtnTapped(_:)))
        navigationItem.rightBarButtonItem = rightNavigationBtn
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        lineItemTableViewHeight.constant = lineItemDetailsTableView.contentSize.height
    }
    
}

//MARK: HIDING UNHIDING VIEWS CONDITIONS
extension NewEstimateVC {
    
    private func hideUnhideClientViews() {
        addClientBtn.isHidden = clientNameLbl.text != ""
        clientChangeBtn.isHidden = clientNameLbl.text == ""
    }
    
}

//MARK: IB ACTIONS IMPLEMENTATION
extension NewEstimateVC {
    
    @objc func saveBtnTapped(_ sender: UIBarButtonItem) {
        print("Save Button Tapped")
    }
    
    @IBAction func addClientBtnTapped(_ sender: UIButton) {
        navigationToCommonAddClientVC()
    }
    
    @IBAction func clientChangeBtnTapped(_ sender: UIButton) {
       navigationToCommonAddClientVC()
    }
    
    @IBAction func addLineItemBtnTapped(_ sender: UIButton) {
        guard let lineItemProductsListVC = StoryBoardNames.ESTIMATE.instantiateViewController(withIdentifier: "LineItemsProductListVC") as? LineItemsProductListVC else {return}
        navigationController?.pushViewController(lineItemProductsListVC, animated: true)
    }
    
}

//MARK: TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension NewEstimateVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LineItemDetailTblCell") as? LineItemDetailTblCell else {return UITableViewCell()}
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

//MARK: VIEW CONTROLLER COMMON IMPLEMENTATIONS - REDUNDANT CODE
extension NewEstimateVC {
    
    func navigationToCommonAddClientVC() {
        guard let commonAddClientVC = StoryBoardNames.COMMON_SCREENS.instantiateViewController(withIdentifier: "CommonAddClientVC") as? CommonAddClientVC else {return}
        commonAddClientVC.modalPresentationStyle = .fullScreen
        commonAddClientVC.clientSelectionCompletionHandler = { [weak self] clientName in
            guard let self = self else {return}
            self.clientNameLbl.text = clientName
            DispatchQueue.main.async { self.hideUnhideClientViews() }
        }
        navigationController?.pushViewController(commonAddClientVC, animated: true)
    }
    
}
