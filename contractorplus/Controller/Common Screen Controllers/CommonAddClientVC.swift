//
//  CommonAddClientVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 02/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class CommonAddClientVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var topTitleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var clientSearchTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var clientSearchTextField: UITextField!
    
    //Completion Handlers
    var clientSelectionCompletionHandler: ((_ selectedClient: String) -> Void)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        topTitleViewHeight.constant = UIDevice.current.hasNotch ? 80 : 60
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        clientSearchTableView.delegate = self
        clientSearchTableView.dataSource = self
        clientSearchTableView.register(UINib(nibName: "CommonAddClientTblCell", bundle: nil), forCellReuseIdentifier: "CommonAddClientTblCell")
        clientSearchTableView.delegate = self
    }

}

//MARK: - IB ACTIONS IMPLEMENTATION
extension CommonAddClientVC {
    
    @IBAction func createNewClientBtnTapped(_ sender: UIButton) {
        guard let createNewClientVC = StoryBoardNames.CLIENT.instantiateViewController(withIdentifier: "NewClientVC") as? NewClientVC else {return}
        navigationController?.pushViewController(createNewClientVC, animated: true)
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - TEXT FIELD DELEGATE IMPLEMENTATION
extension CommonAddClientVC: UITextFieldDelegate {
    
    @IBAction func textFieldEditingChanged(_ textfield: UITextField) {
        print(textfield.text)
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        return true
    }
    
}

//MARK: - TABLE VIEW DELEGATE & DATA SOURCE IMPLEMENTATION
extension CommonAddClientVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommonAddClientTblCell") as? CommonAddClientTblCell else {return UITableViewCell()}
        cell.configureCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedClient = "Ajinkya Sonar"
        guard let completionHandler = clientSelectionCompletionHandler else {return}
        completionHandler(selectedClient)
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
