//
//  NewClientVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 30/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class NewClientVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Basic Outlets
    @IBOutlet weak var basic_FirstNameTextField: UITextField!
    @IBOutlet weak var basic_LastNameTextField: UITextField!
    @IBOutlet weak var basic_CompanyNameTextField: UITextField!
    
    //Contact Info Outlets
    @IBOutlet weak var contact_PhoneNumberTextField: UITextField!
    @IBOutlet weak var contact_EmailAddressTextField: UITextField!
    
    //Billing Outlets
    @IBOutlet weak var billing_TextFieldsStack: UIStackView!
    
    
    //Variables
    var pickedContact: CNContact?
    var isBillingAddressSamePropertyAddress = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "New Client"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
    }
    
    private func initialUISetup() {
        addNavigationBarButtonItems()
    }
    
    private func addNavigationBarButtonItems() {
        let rightNavigationBtn = UIBarButtonItem(barButtonSystemItem: .save,
                                                 target: self,
                                                 action: #selector(saveBtnTapped(_:)))
        navigationItem.rightBarButtonItem = rightNavigationBtn
    }
    
}

//MARK: IB ACTIONS IMPLEMENTATION
extension NewClientVC {
    
    @objc func saveBtnTapped(_ sender: UIBarButtonItem) {
        print("Save Button Tapped")
    }
    
    @IBAction func addContactBtnTapped(_ sender: UIButton) {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        present(contactPicker, animated: true, completion: nil)
    }
    
    @IBAction func sameAsPropertyAddressBtnTapped(_ sender: UIButton) {
        isBillingAddressSamePropertyAddress = !isBillingAddressSamePropertyAddress
        
        if isBillingAddressSamePropertyAddress {
            sender.setImage(AppImages.SELECTED_CKECKMARK, for: .normal)
        }
        else {
            sender.setImage(AppImages.UNSELECT_CHECKMARK, for: .normal)
            let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height + billing_TextFieldsStack.bounds.size.height)
            self.scrollView.setContentOffset(bottomOffset, animated: true)
        }
        
        billing_TextFieldsStack.isHidden = isBillingAddressSamePropertyAddress
        
    }
    
}

//MARK: CONTACT PICKER DELEGATE IMPLEMENTATION
extension NewClientVC: CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        pickedContact = contact
        guard let pickedContact = pickedContact else {return}
        basic_FirstNameTextField.text = pickedContact.givenName
        basic_LastNameTextField.text = pickedContact.familyName
        contact_PhoneNumberTextField.text = (pickedContact.phoneNumbers.first?.value)?.stringValue
        basic_CompanyNameTextField.text = pickedContact.organizationName
        if (pickedContact.emailAddresses.first?.value) != nil {
            contact_EmailAddressTextField.text = (pickedContact.emailAddresses.first?.value)! as String
        }
    }
    
}


