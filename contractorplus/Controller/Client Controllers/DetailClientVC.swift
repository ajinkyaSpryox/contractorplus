//
//  DetailClientVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 01/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit
import CarbonKit

class DetailClientVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var topDisplayView: UIView!
    @IBOutlet weak var topDisplayViewHeight: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCarbonKit()
    }

}

//MARK: - IB ACTIONS IMPLEMENTATION
extension DetailClientVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - CARBON TAB SETUP & DELEGATE IMPLEMENTATION
extension DetailClientVC: CarbonTabSwipeNavigationDelegate {
   
    private func setupCarbonKit() {
        let menuItems = ["Profile", "Contacts", "Properties", "Notes", "Statements", "Invoices", "Payments"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: menuItems, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.view.translatesAutoresizingMaskIntoConstraints = false
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.backgroundColor = .white
        carbonTabSwipeNavigation.setTabBarHeight(50)
        carbonTabSwipeNavigation.setIndicatorColor(.systemRed)
        carbonTabSwipeNavigation.setNormalColor(.secondaryLabel, font: UIFont(name: "MyriadPro-Semibold", size: 16)!)
        carbonTabSwipeNavigation.setSelectedColor(.secondaryLabel, font: UIFont(name: "MyriadPro-Semibold", size: 16)!)
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation.view ?? "", attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: topDisplayView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: topDisplayViewHeight.constant))
        
        self.view.addConstraint(NSLayoutConstraint(item: carbonTabSwipeNavigation.view ?? "", attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view , attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0))
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let index = Int(index)
        switch index {
        case 0:
            //Display Segmented Profile View Controller
            guard let segmentedProfileVC = StoryBoardNames.CLIENT_DETAIL_SEGMENTS.instantiateViewController(withIdentifier: "SegmentedClientProfileVC") as? SegmentedClientProfileVC else {return UIViewController()}
            return segmentedProfileVC
            
        default:
            return UIViewController()
        }
        
    }
    
}
