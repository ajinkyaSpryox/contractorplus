//
//  ListClientVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 30/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit
import SideMenu

class ListClientVC: UIViewController {
    
    //Outlets
    var floatingActionBtn: ActionButton!
    @IBOutlet weak var listClientTableView: UITableView!
    
    //Searching Variables
    var isSearching = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.setHidesBackButton(true, animated: true)
        title = "Client List"
        addNavigationBarButtonItems()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.setHidesBackButton(false, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
    }
    
    private func initialUISetup() {
        listClientTableView.delegate = self
        listClientTableView.dataSource = self
        listClientTableView.register(UINib(nibName: "ListClientTblCell", bundle: nil), forCellReuseIdentifier: "ListClientTblCell")
        setupFloatingActionButton()
    }
    
    private func addNavigationBarButtonItems() {
        
        let leftNavigationBtn = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(menuBtnTapped(_:)))
        leftNavigationBtn.image = AppImages.MENU
        navigationItem.leftBarButtonItem = leftNavigationBtn
        
        let rightSearchNavigationBtn = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(searchBtnTapped(_:)))
        rightSearchNavigationBtn.image = AppImages.SEARCH
        navigationItem.rightBarButtonItems = [rightSearchNavigationBtn]

    }
    
    private func setupFloatingActionButton() {
        floatingActionBtn = ActionButton(attachedToView: self.view, items: nil)
        floatingActionBtn.setTitle("+", forState: .normal)
        floatingActionBtn.backgroundColor = #colorLiteral(red: 0.9097405076, green: 0.1442055702, blue: 0.1389430463, alpha: 1)
        floatingActionBtn.action = { [weak self] action in
            guard let self = self else {return}
            guard let newClientVC = self.storyboard?.instantiateViewController(withIdentifier: "NewClientVC") as? NewClientVC else {return}
            self.navigationController?.pushViewController(newClientVC, animated: true)
        }
    }

}

//MARK: IB-ACTIONS IMPLEMENTATION
extension ListClientVC {
    
    @objc
    func menuBtnTapped(_ sender: UIBarButtonItem) {
        
        guard let optionsMenuVC = StoryBoardNames.SIDEMENU.instantiateViewController(withIdentifier: "SideMenuOptionsVC") as? SideMenuOptionsVC else {return}
        let menu = SideMenuNavigationController(rootViewController: optionsMenuVC)
        menu.leftSide = true
        menu.statusBarEndAlpha = 0
        menu.menuWidth = self.view.bounds.width - 120
        present(menu, animated: true, completion: nil)
    }
    
    @objc
    func searchBtnTapped(_ sender: UIBarButtonItem) {
        #warning("Navigation to common Search - to be planned")
    }
    
}

extension ListClientVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListClientTblCell") as? ListClientTblCell else {return UITableViewCell()}
        cell.configureCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let clientDetailVC = storyboard?.instantiateViewController(withIdentifier: "DetailClientVC") as? DetailClientVC else {return}
        navigationController?.pushViewController(clientDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
