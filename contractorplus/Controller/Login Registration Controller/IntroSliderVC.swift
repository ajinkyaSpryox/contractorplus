//
//  IntroSliderVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 04/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class IntroSliderVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var pageViewController: UIPageControl!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
    }
    
    private func initialUISetup() {
        sliderCollectionView.delegate = self
        sliderCollectionView.dataSource = self
        sliderCollectionView.register(UINib(nibName: "IntroSliderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "IntroSliderCollectionCell")
        
        pageViewController.numberOfPages = sliderImagesArr.count
    }

}

//MARK: - IB ACTIONS IMPLEMENTATION
extension IntroSliderVC {
    
    @IBAction func loginBtnTapped(_ sender: UIButton) {
        guard let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {return}
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func registrationBtnTapped(_ sender: UIButton) {
        guard let registerVC = storyboard?.instantiateViewController(withIdentifier: "Registration1VC") as? Registration1VC else {return}
        navigationController?.pushViewController(registerVC, animated: true)
    }
    
}

extension IntroSliderVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderImagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroSliderCollectionCell", for: indexPath) as? IntroSliderCollectionCell else {return UICollectionViewCell()}
        cell.configureCell(sliderImage: sliderImagesArr[indexPath.item], indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageViewController.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height - 20)
    }
    
}
