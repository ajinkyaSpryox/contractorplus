//
//  Registration2VC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 29/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class Registration2VC: UIViewController {
    
    //Outlets
    @IBOutlet weak var companySizeSegmentController: CustomSegmentedControl!
    @IBOutlet weak var yearsInBusinessSegmentController: CustomSegmentedControl!
    @IBOutlet weak var selectIndustryTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        setupCompanySegmentUI()
        setupYearsInBusinessSegmentUI()
    }

}

//MARK: - SETUP CUSTOM SEGMENT CONTROLLERS
extension Registration2VC {
    
    private func setupCompanySegmentUI() {
        let companySegmentOptions = ["Just Me", "2-3", "4-10", "10+"]
        companySizeSegmentController.setButtonTitles(buttonTitles: companySegmentOptions)
        companySizeSegmentController.selectedButtonBgColor = #colorLiteral(red: 0.9401887655, green: 0.07712750882, blue: 0.07783869654, alpha: 1)
        companySizeSegmentController.selectorViewColor = .clear
        companySizeSegmentController.selectorTextColor = .white
        companySizeSegmentController.buttonBorderWidth = 0.3
        companySizeSegmentController.buttonBorderColor = .systemGray
    }
    
    private func setupYearsInBusinessSegmentUI() {
        let businessYearsSegmentOptions = ["<1", "1-2", "3-10", "10+"]
        yearsInBusinessSegmentController.setButtonTitles(buttonTitles: businessYearsSegmentOptions)
        yearsInBusinessSegmentController.selectedButtonBgColor = #colorLiteral(red: 0.9401887655, green: 0.07712750882, blue: 0.07783869654, alpha: 1)
        yearsInBusinessSegmentController.selectorViewColor = .clear
        yearsInBusinessSegmentController.selectorTextColor = .white
        yearsInBusinessSegmentController.buttonBorderWidth = 0.3
        yearsInBusinessSegmentController.buttonBorderColor = .systemGray
    }
    
}

//MARK: - IB ACTIONS
extension Registration2VC {
    
    @IBAction func startNowBtnTapped(_ sender: UIButton) {
        guard let listClientVC = StoryBoardNames.CLIENT.instantiateViewController(withIdentifier: "ListClientVC") as? ListClientVC else {return}
        navigationController?.pushViewController(listClientVC, animated: true)
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
