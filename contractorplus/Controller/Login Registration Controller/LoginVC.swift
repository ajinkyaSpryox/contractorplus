//
//  LoginVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 29/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit
import Alamofire
import GoogleSignIn
import Toast_Swift

class LoginVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var googleLoginInStack: UIStackView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
    }
    
    private func initialUISetup() {
        let googleLoginTapGesture = UITapGestureRecognizer(target: self, action: #selector(loginUserWithGoogleAccount(_:)))
        googleLoginInStack.addGestureRecognizer(googleLoginTapGesture)
        
    }
    
}

//MARK: IB_ACTIONS
extension LoginVC {
    
    @IBAction func appLoginBtnTapped(_ sender: UIButton) {
        guard let email = emailTextField.text, let password = passwordTextField.text else { return }
        appLoginWith(email: email, password: password)
    }
    
    @IBAction func signupBtnTapped(_ sender: UIButton) {
        guard let registration1VC = storyboard?.instantiateViewController(withIdentifier: "Registration1VC") as? Registration1VC else {return}
        navigationController?.pushViewController(registration1VC, animated: true)
    }
    
    @objc
    func loginUserWithGoogleAccount(_ sender: UITapGestureRecognizer) {
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
}

//MARK: - GOOGLE SIGN IN IMPLEMENTATION
extension LoginVC: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        //let userId = user.userID                  // For client-side use only!
        //let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        //let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        
        print(email ?? "")
        print(fullName ?? "")
        print(familyName ?? "")
        
    }
    
}

//MARK: - API SERVICE IMPLEMENTATION
extension LoginVC {
    
    func appLoginWith(email: String, password: String) {
        
        let parameters: [String: Any] = ["email" : email, "password" : password]
        let headers: HTTPHeaders = [ "Content-type": "multipart/form-data"]
        
        self.view.makeToastActivity(.center)
        
        AF.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, to: AppUrls.LOGIN_URL, method: .post, headers: headers).validate()
            .responseJSON(completionHandler: { [weak self] (response) in
                
                guard let self = self else {return}
                
                if response.error != nil {
                    self.view.hideToastActivity()
                    self.view.makeToast(WebServiceErrorMessages.NO_INTERNET_AVAILABLE.rawValue, duration: 2.0, position: .bottom)
                }
                else {
                    guard let data = response.data else {return}
                    let decoder = JSONDecoder()
                    
                    do{
                        let returnedResponse = try decoder.decode(Login.self, from: data)
                        
                        if returnedResponse.success == true {
                            //Success
                            self.view.hideToastActivity()
                            print(returnedResponse.data)
                        }
                        else {
                            //Failure
                            self.view.hideToastActivity()
                            self.view.makeToast(returnedResponse.message, duration: 2.0, position: .bottom)
                        }
                        
                    }catch{
                        self.view.hideToastActivity()
                        self.view.makeToast(WebServiceErrorMessages.WRONG_CREDENTAILS.rawValue, duration: 2.0, position: .bottom)
                    }
                    
                }
                
            })
        
    }
    
}
