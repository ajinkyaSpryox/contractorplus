//
//  RegistrationVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 29/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class Registration1VC: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

}

//MARK: IB ACTIONS
extension Registration1VC {
    
    @IBAction func loginBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
