//
//  SideMenuOptionsVC.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 30/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class SideMenuOptionsVC: UIViewController {
    
    @IBOutlet weak var optionsMenuTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialUISetup()
    }
    
    private func initialUISetup() {
        optionsMenuTableView.delegate = self
        optionsMenuTableView.dataSource = self
        optionsMenuTableView.register(UINib(nibName: "SideMenuOptionTblCell", bundle: nil), forCellReuseIdentifier: "SideMenuOptionTblCell")
    }

}

extension SideMenuOptionsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuOptionsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = optionsMenuTableView.dequeueReusableCell(withIdentifier: "SideMenuOptionTblCell") as? SideMenuOptionTblCell else {return UITableViewCell()}
        cell.configureCell(sideMenuOption: sideMenuOptionsArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 3:
            //Go to Estimate List View Controller
            guard let estimateListVC = StoryBoardNames.ESTIMATE.instantiateViewController(withIdentifier: "ListEstimateVC") as? ListEstimateVC else {return}
            navigationController?.pushViewController(estimateListVC, animated: true)
            
        case 5:
            //Go to List Shopping List View Controller
            guard let listShoppingListVC = StoryBoardNames.SHOPPING_LIST.instantiateViewController(withIdentifier: "ListShoppingListVC") as? ListShoppingListVC else {return}
            navigationController?.pushViewController(listShoppingListVC, animated: true)
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
