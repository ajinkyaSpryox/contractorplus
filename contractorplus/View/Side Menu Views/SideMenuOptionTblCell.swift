//
//  SideMenuOptionTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 30/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class SideMenuOptionTblCell: UITableViewCell {
    
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var optionTitleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(sideMenuOption: SideMenuOption) {
        iconImageView.image = sideMenuOption.icon
        optionTitleLbl.text = sideMenuOption.title
    }
    
}
