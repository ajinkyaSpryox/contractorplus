//
//  ListClientTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 30/04/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class ListClientTblCell: UITableViewCell {
    
    @IBOutlet private weak var clientImageView: UIImageView!
    @IBOutlet private weak var clientNameLbl: UILabel!
    @IBOutlet private weak var companyNameLbl: UILabel!
    @IBOutlet private weak var cityNameLbl: UILabel!
    @IBOutlet private weak var stateNameLbl: UILabel!
    @IBOutlet private weak var daysLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell() {
        clientImageView.image = AppImages.DEFAULT_AVATAR
        clientNameLbl.text = "Ajinkya Sonar"
        companyNameLbl.text = "Spryox"
        cityNameLbl.text = "Mumbai,"
        stateNameLbl.text = "Maharashtra"
        daysLbl.text = "12 days Ago"
    }
    
}
