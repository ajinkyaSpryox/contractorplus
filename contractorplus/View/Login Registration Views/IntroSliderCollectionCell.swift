//
//  IntroSliderCollectionCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 04/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class IntroSliderCollectionCell: UICollectionViewCell {
    
    @IBOutlet private weak var sliderImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(sliderImage: SliderImage, indexPath: IndexPath) {
        sliderImageView.image = sliderImage.sliderImageName
    }

}
