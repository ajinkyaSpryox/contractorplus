//
//  SegmentedEstimateLineItemTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 06/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class SegmentedEstimateLineItemTblCell: UITableViewCell {
    
    @IBOutlet private weak var outerView: UIView!
    
    //Ouantity View Outlets
    @IBOutlet private weak var quantityOuterView: UIView!
    @IBOutlet private weak var  quantityTitleLabel: UILabel!
    @IBOutlet private weak var itemQuantityLbl: UILabel!
    
    //Unit Cost View Outlets
    @IBOutlet private weak var unitCostOuterView: UIView!
    @IBOutlet private weak var unitCostTitleLabel: UILabel!
    @IBOutlet private weak var unitCostSymbolLbl: UILabel!
    @IBOutlet private weak var unitCostAmountLbl: UILabel!
    
    //Tax View Outlets
    @IBOutlet private weak var taxOuterView: UIView!
    @IBOutlet private weak var taxTitleLabel: UILabel!
    @IBOutlet private weak var cgstTaxAmountSymbolLbl: UILabel!
    @IBOutlet private weak var cgstTaxAmountLbl: UILabel!
    
    @IBOutlet private weak var sGstTaxAmountSymbolLbl: UILabel!
    @IBOutlet private weak var sGstTaxAmountLbl: UILabel!
    
    
    //Total View Outlets
    @IBOutlet private weak var totalOuterView: UIView!
    @IBOutlet private weak var totalTitleLabel: UILabel!
    @IBOutlet private weak var totalAmountSymbolLbl: UILabel!
    @IBOutlet private weak var totalAmountLbl: UILabel!
    
    //Other Outlets
    @IBOutlet private weak var itemNameLbl: UILabel!
    @IBOutlet private weak var itemDetailLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        outerView.addBorders(edges: [.top, .bottom], color: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1), thickness: 0.5)
        
        quantityOuterView.backgroundColor = .white
        quantityOuterView.addBorders(edges: [.right], color: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1), thickness: 0.5)
        quantityTitleLabel.addBorders(edges: [.bottom], color: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1), thickness: 0.5)
        
        unitCostOuterView.backgroundColor = .white
        unitCostOuterView.addBorders(edges: [.right], color: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1), thickness: 0.5)
        unitCostTitleLabel.addBorders(edges: [.bottom], color: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1), thickness: 0.5)
        
        taxOuterView.backgroundColor = .white
        taxOuterView.addBorders(edges: [.right], color: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1), thickness: 0.5)
        taxTitleLabel.addBorders(edges: [.bottom], color: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1), thickness: 0.5)
        
        totalOuterView.backgroundColor = .white
        totalTitleLabel.addBorders(edges: [.bottom], color: #colorLiteral(red: 0.6392156863, green: 0.6392156863, blue: 0.6392156863, alpha: 1), thickness: 0.5)
    }
    
    
    func configureCell() {
        itemNameLbl.text = "Window"
        itemDetailLbl.text = "Clean all exterior windows on upper level"
        itemQuantityLbl.text = "1"
        unitCostSymbolLbl.text = "$"
        cgstTaxAmountSymbolLbl.text = "$"
        sGstTaxAmountSymbolLbl.text = "$"
        totalAmountSymbolLbl.text = "$"
        unitCostAmountLbl.text = "20"
        cgstTaxAmountLbl.text = "10"
        sGstTaxAmountLbl.text = "10"
        totalAmountLbl.text = "40"
    }
    
}
