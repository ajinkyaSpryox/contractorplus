//
//  SegmentEstimateMessageTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 05/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class SegmentEstimateMessageTblCell: UITableViewCell {
    
    @IBOutlet private weak var titleLbl: UILabel!
    @IBOutlet private weak var detailLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell() {
        titleLbl.text = "Ajinkya Sonar"
        detailLbl.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    }
    
}
