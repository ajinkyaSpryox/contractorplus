//
//  ListEstimateTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 05/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class ListEstimateTblCell: UITableViewCell {
    
    @IBOutlet private weak var estimateStatusBtn: UIButton!
    @IBOutlet private weak var estimateNumberLbl: UILabel!
    @IBOutlet private weak var estimateDetailLbl: UILabel!
    @IBOutlet private weak var estimateNameLbl: UILabel!
    @IBOutlet private weak var estimateDateLbl: UILabel!
    @IBOutlet private weak var estimateAmountSymbolLbl: UILabel!
    @IBOutlet private weak var estimateAmountLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        estimateStatusBtn.isUserInteractionEnabled = false
        estimateStatusBtn.clipsToBounds = true
        estimateStatusBtn.layer.cornerRadius = 8
        estimateStatusBtn.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func configureCell() {
        estimateNumberLbl.text = "Est-00001"
        estimateDetailLbl.text = "Interior Windows"
        estimateNameLbl.text = "Ajinkya Sonar"
        estimateDateLbl.text = "01 March 2020 06.20"
        estimateAmountSymbolLbl.text = "$"
        estimateAmountLbl.text = "28.00"
        estimateStatusBtn.setTitle("Accepted", for: .normal)
    }
    
}
