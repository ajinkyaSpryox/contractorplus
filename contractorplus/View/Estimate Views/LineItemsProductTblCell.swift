//
//  CommonLineItemsProductTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 02/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class LineItemsProductTblCell: UITableViewCell {
    
    @IBOutlet private weak var lineItemNameLbl: UILabel!
    @IBOutlet private weak var lineItemDescriptionLbl: UILabel!
    @IBOutlet private weak var lineItemCurrencyLbl: UILabel!
    @IBOutlet private weak var lineItemAmountLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell() {
        lineItemNameLbl.text = "Exterior High-Level Windows"
        lineItemDescriptionLbl.text = "Clean all exterior windows on upper levels"
        lineItemCurrencyLbl.text = "$"
        lineItemAmountLbl.text = "0.00"
    }
    
}
