//
//  LineItemDetailTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 01/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class LineItemDetailTblCell: UITableViewCell {
    
    @IBOutlet private weak var detailContainerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        detailContainerView.backgroundColor = .clear
    }
    
}
