//
//  ShoppingListItemsSearchTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 06/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class ShoppingListItemsSearchTblCell: UITableViewCell {
    
    @IBOutlet private weak var itemImageView: UIImageView!
    @IBOutlet private weak var supplierNameLbl: UILabel!
    @IBOutlet private weak var itemNameLbl: UILabel!
    @IBOutlet private weak var itemAmountSymbolLbl: UILabel!
    @IBOutlet private weak var itemAmountLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell() {
        supplierNameLbl.text = "ABC Hardware"
        itemNameLbl.text = "Lev Light Bulb"
        itemAmountSymbolLbl.text = "$"
        itemAmountLbl.text = "10.00"
    }
    
}
