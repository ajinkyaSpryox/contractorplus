//
//  CommonAddClientTblCell.swift
//  contractorplus
//
//  Created by Ajinkya Sonar on 02/05/20.
//  Copyright © 2020 Ajinkya Sonar. All rights reserved.
//

import UIKit

class CommonAddClientTblCell: UITableViewCell {
    
    @IBOutlet private weak var clientNameLbl: UILabel!
    @IBOutlet private weak var propertyInfoLbl: UILabel!
    @IBOutlet private weak var clientRoleLbl: UILabel!
    @IBOutlet private weak var clientImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell() {
        clientImageView.image = AppImages.AVATAR_TEAL
        clientNameLbl.text = "Ajinkya Sonar"
        propertyInfoLbl.text = "2011 Scheduling St."
        clientRoleLbl.text = "LEAD"
    }
    
}
